package request

import (
	"bufio"
	"encoding/json"
	"log"
	"math/rand"
	"os"
)

const sizeLimit = 5000

// Request describes the structure of data logged for every request
type Request struct {
	RequestId  string `json:"requestID"`
	UserId     int    `json:"userID"`
	CustomerId int    `json:"customerID"`
	UserAgent  string `json:"userAgent"`
	Url        string `json:"url"`
	Timestamp  int64  `json:"timestamp"`
}

// SamplerInterface describes necessary methods to generate random requests samples
type SamplerInterface interface {
	SampleRequests(pathToJsonFile string) []Request
}

// SampleRequests generate random sample of requests from the give given by pathToJsonFile
// The sample should not include more then sizeLimit of request for every customer
func SampleRequests(pathToJsonFile string) []Request {
	requestCounter := calculateRequestsPerCustomer(pathToJsonFile)

	return getRandomRequests(pathToJsonFile, requestCounter)
}

func calculateRequestsPerCustomer(pathToJsonFile string) map[int]int {
	file, err := os.Open(pathToJsonFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	fileScanner := bufio.NewScanner(file)

	requestCounter := map[int]int{}
	for fileScanner.Scan() {
		req := readSingleLine(fileScanner)
		requestCounter[req.CustomerId]++
	}

	return requestCounter
}

func getRandomRequests(pathToJsonFile string, counter map[int]int) []Request {
	file, err := os.Open(pathToJsonFile)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	fileScanner := bufio.NewScanner(file)

	requests := []Request{}
	for fileScanner.Scan() {
		req := readSingleLine(fileScanner)
		if shouldBeIncluded(req, counter) {
			requests = append(requests, *req)
		}
	}

	return requests
}

func shouldBeIncluded(req *Request, counter map[int]int) bool {
	count, ok := counter[req.CustomerId]
	if !ok {
		return false
	}

	if count <= sizeLimit {
		return true
	}

	percentToInculde := float32(sizeLimit) / float32(count) * 100
	randNum := rand.Intn(100)

	return randNum <= int(percentToInculde)
}

func readSingleLine(fileScanner *bufio.Scanner) *Request {
	jsonReq := fileScanner.Bytes()
	req := &Request{}
	err := json.Unmarshal(jsonReq, &req)
	if err != nil {
		log.Println(err)
		return nil
	}

	return req
}
