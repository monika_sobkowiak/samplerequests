package request

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type includeTestCase struct {
	request *Request
	counter map[int]int
	result  bool
}

var request = &Request{
	RequestId:  "A",
	UserId:     1,
	CustomerId: 1,
	UserAgent:  "Firefox",
	Url:        "http://theadex.com",
	Timestamp:  1470652067,
}

var shouldBeIncludedCases = []includeTestCase{
	{request, map[int]int{1: 4000}, true},
	{request, map[int]int{}, false},
}

func Test_ShouldBeIncluded(t *testing.T) {
	for index, testCase := range shouldBeIncludedCases {
		assert.Equal(t, testCase.result, shouldBeIncluded(testCase.request, testCase.counter), fmt.Sprintf("case: %d failed", index))
	}
}

var expectedRequestCounter = map[int]int{
	2: 4,
	3: 2,
	1: 3,
}

func Test_CalculateRequestsPerCustomer(t *testing.T) {
	requestCounter := calculateRequestsPerCustomer("test_data.json")
	assert.Equal(t, expectedRequestCounter, requestCounter)
}
